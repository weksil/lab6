create table test(id serial, name varchar(64));
create subscription test_pub connection 'dbname=postgres user=postgres host=lab6_db-master_1 port=5432' publication test_pub;

create extension dblink;
create foreign data wrapper postgresql validator postgresql_fdw_validator;
create server master_logical foreign data wrapper postgresql options (host 'lab6_db-master_1', dbname 'postgres', port '5432');
create user mapping for public server master_logical;