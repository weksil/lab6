docker-compose up -d;
docker cp master_config/postgresql.conf lab6_db-master_1:/var/lib/postgresql/data/postgresql.conf;
docker cp master_config/pg_hba.conf lab6_db-master_1:/var/lib/postgresql/data/pg_hba.conf;
docker cp master_config/master.sql lab6_db-master_1:/home/master.sql;
docker cp slave_replica_config/postgresql.conf lab6_db-slave_1:/var/lib/postgresql/postgresql.conf;
docker cp slave_logical_config/postgresql.conf lab6_db-slave-logical_1:/var/lib/postgresql/data/postgresql.conf;
docker cp slave_logical_config/slave.sql lab6_db-slave-logical_1:/home/slave.sql;
docker-compose restart;

sleep 10;
docker exec lab6_db-slave_1 bash -c "cd /var/lib/postgresql/data ; rm -r * ; pg_basebackup -D . -R -P -U postgres -w  --wal-method=stream -h lab6_db-master_1 -p 5432 ; cp ../postgresql.conf postgresql.conf";
docker exec lab6_db-master_1 bash -c "psql -U postgres -f /home/master.sql";
docker exec lab6_db-slave-logical_1 bash -c "psql -U postgres -f /home/slave.sql";